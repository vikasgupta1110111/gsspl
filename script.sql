USE [master]
GO
/****** Object:  Database [VikasGupta]    Script Date: 20-Mar-21 8:36:39 PM ******/
CREATE DATABASE [VikasGupta]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'VikasGupta', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\VikasGupta.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'VikasGupta_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL14.MSSQLSERVER\MSSQL\DATA\VikasGupta_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [VikasGupta] SET COMPATIBILITY_LEVEL = 140
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [VikasGupta].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [VikasGupta] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [VikasGupta] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [VikasGupta] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [VikasGupta] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [VikasGupta] SET ARITHABORT OFF 
GO
ALTER DATABASE [VikasGupta] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [VikasGupta] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [VikasGupta] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [VikasGupta] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [VikasGupta] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [VikasGupta] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [VikasGupta] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [VikasGupta] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [VikasGupta] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [VikasGupta] SET  DISABLE_BROKER 
GO
ALTER DATABASE [VikasGupta] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [VikasGupta] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [VikasGupta] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [VikasGupta] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [VikasGupta] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [VikasGupta] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [VikasGupta] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [VikasGupta] SET RECOVERY FULL 
GO
ALTER DATABASE [VikasGupta] SET  MULTI_USER 
GO
ALTER DATABASE [VikasGupta] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [VikasGupta] SET DB_CHAINING OFF 
GO
ALTER DATABASE [VikasGupta] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [VikasGupta] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [VikasGupta] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'VikasGupta', N'ON'
GO
ALTER DATABASE [VikasGupta] SET QUERY_STORE = OFF
GO
USE [VikasGupta]
GO
/****** Object:  User [sa1]    Script Date: 20-Mar-21 8:36:39 PM ******/
CREATE USER [sa1] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[City_Master]    Script Date: 20-Mar-21 8:36:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[City_Master](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CityName] [varchar](50) NULL,
	[StateId] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[State_Master]    Script Date: 20-Mar-21 8:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[State_Master](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StateName] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student_Marks_Master]    Script Date: 20-Mar-21 8:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student_Marks_Master](
	[Id] [int] NULL,
	[StudentId] [int] NULL,
	[Subject1] [int] NULL,
	[Subject2] [int] NULL,
	[Subject3] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Student_Master]    Script Date: 20-Mar-21 8:36:40 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Student_Master](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[StudentName] [varchar](50) NULL,
	[CityId] [int] NULL,
	[State] [int] NULL,
	[RollNo] [bigint] NULL,
	[Gender] [varchar](50) NULL,
	[EmailId] [varchar](50) NULL,
	[Mobile] [bigint] NULL,
	[Address] [varchar](50) NULL
) ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateInsert_Employee]    Script Date: 20-Mar-21 8:36:40 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- Add a row into the "MSagent_parameters" table
CREATE procedure [dbo].[sp_CreateInsert_Employee] (
    @name varchar(50),
    @phone nvarchar(255),
	@Email varchar(50)
	)
as
    begin
	insert crudtbl (Name,Phone,Email)values(@name,@phone,@Email)
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_Delete_Employee]    Script Date: 20-Mar-21 8:36:40 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- Add a row into the "MSagent_parameters" table
Create procedure [dbo].[sp_Delete_Employee] (
    @id int
	)
as
    begin
	Delete from crudtbl where Id=@id
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_find_Employee]    Script Date: 20-Mar-21 8:36:40 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- Add a row into the "MSagent_parameters" table
create procedure [dbo].[sp_find_Employee] (@id int)
as
    begin
	select * from crudtbl where Id=@id 
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_select_Employee]    Script Date: 20-Mar-21 8:36:40 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- Add a row into the "MSagent_parameters" table
create procedure [dbo].[sp_select_Employee] 
as
    begin
	select * from crudtbl 
	end
GO
/****** Object:  StoredProcedure [dbo].[sp_Update_Employee]    Script Date: 20-Mar-21 8:36:40 PM ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO

-- Add a row into the "MSagent_parameters" table
create procedure [dbo].[sp_Update_Employee] (
    @id int,
    @name varchar(50),
    @phone nvarchar(255),
	@Email varchar(50)
	)
as
    begin
	update crudtbl set Name=@name,Phone=@phone,Email=@Email where Id=@id
	end
GO
USE [master]
GO
ALTER DATABASE [VikasGupta] SET  READ_WRITE 
GO
